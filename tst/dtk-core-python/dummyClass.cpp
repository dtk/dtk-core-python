// dummyClass.cpp ---

#include "dummyClass.h"

namespace dummy {

    namespace detail {

        DummyFactory _f;

    }

    DummyFactory *factory(void) { return &detail::_f; }
}

int DummyClass::nbParams(void)
{
    return this->parameters().size();
}

void DummyFactory::record(DummyPlugin *p)
{
    m_plugin = p;
}

DummyClass *DummyFactory::create(void)
{
    if (m_current) {
        delete m_current;
    }
    m_current =  static_cast<DummyClass *>(m_plugin->create());
    return m_current;
}

DummyClass *DummyFactory::current(void) const
{
    return m_current;
}

//
// dummyClass.cpp ends here
