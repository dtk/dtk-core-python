#include "dtkCoreParametersWrappingTest.h"

#include <dtkCore>
#include <dtkScript>

#include "dummyClass.h"

dtkCoreParametersWrappingTestCase::dtkCoreParametersWrappingTestCase(void)
{
}

dtkCoreParametersWrappingTestCase::~dtkCoreParametersWrappingTestCase(void)
{
}

void dtkCoreParametersWrappingTestCase::initTestCase(void)
{
    QFile f(QFINDTESTDATA("./dummyclassimpl.py"));
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "cant open python file";
    }

    QTextStream ss(&f);

    dtkScriptInterpreterPython::instance()->init();
    int stat;
    dtkScriptInterpreterPython::instance()->interpret(ss.readAll(), &stat);
}

void dtkCoreParametersWrappingTestCase::init(void)
{
}

void dtkCoreParametersWrappingTestCase::creation(void)
{
    DummyClass *the_class = dummy::factory()->create();
    Q_ASSERT(the_class);

    dtkCoreParameters params = the_class->parameters();
    QVERIFY(params.size() == 3);

    auto type_bool = QMetaType::fromType<dtk::d_bool>().name();
    auto type_int  = QMetaType::fromType<dtk::d_int>().name();
    auto type_real = QMetaType::fromType<dtk::d_real>().name();

    QCOMPARE(type_bool, params["a_bool"]->typeName());
    QCOMPARE(type_int , params["a_int" ]->typeName());
    QCOMPARE(type_real, params["a_real"]->typeName());
}

void dtkCoreParametersWrappingTestCase::addParams(void)
{
    auto d = dummy::factory()->current();
    dtkCoreParameterPath *path = new dtkCoreParameterPath("test_file", QFINDTESTDATA("./addParams.py"), {"*.py"}, "Test file for testing add params");

    d->addPath("a_path", path);
    QFile f(path->path());
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "cant open python file";
    }
    QTextStream ss(&f);
    int stat;
    dtkScriptInterpreterPython::instance()->interpret(ss.readAll(), &stat);
    QVERIFY(stat == 0);

    auto&& inliststringlist = dynamic_cast<dtkCoreParameterInListStringList &>(*(d->parameters()["a_inliststringlist"]));

    QVERIFY(&inliststringlist);
    QCOMPARE(inliststringlist.size(), 5);
    QCOMPARE(inliststringlist.value().size(), 2);
    QCOMPARE(inliststringlist.value()[0], "tata");
    QCOMPARE(inliststringlist.value()[1], "tutu");
}

void dtkCoreParametersWrappingTestCase::pingPong(void)
{
    QFile f(QFINDTESTDATA("./pingPong.py"));
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "cant open python file";
    }

    auto d = dummy::factory()->current();
    auto params = d->parameters();
    auto&& real = dynamic_cast<dtk::d_real &>(*params["a_real"]);
    real = 3.14159;
    auto&& integer = dynamic_cast<dtk::d_int &>(*params["a_int"]);
    integer = 9;
    auto&& boolean = dynamic_cast<dtk::d_bool &>(*params["a_bool"]);
    boolean = true;

    QTextStream ss(&f);
    int stat;
    dtkScriptInterpreterPython::instance()->interpret(ss.readAll(), &stat);
    QVERIFY(stat == 0);

    QCOMPARE(real.value(), 1.414);
    QCOMPARE(integer.value(), 3);
    QCOMPARE(boolean.value(), false);
}

void dtkCoreParametersWrappingTestCase::pongPing(void)
{
    QFile f(QFINDTESTDATA("./pongPing.py"));
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "cant open python file";
    }

    auto d = dummy::factory()->current();

    dtkCoreParameters toto;
    toto["b_int"] = new dtk::d_int(1001);
    toto["b_bool"] = new dtk::d_bool(false);
    toto["b_real"] = new dtk::d_real(1.414);
    toto["b_string"] = new dtk::d_string("string", "My taylor is rich");
    toto["b_inlist"] = new dtk::d_inliststring("cell type", "", QStringList({"Ganglion", "CNS"}), "Select the cell type");
    toto["b_range"] = new dtk::d_range_real("range", std::array<double, 2>({-1.5, 1.5}), -3.0, 3.0);
    toto["b_inlistslist"] = new dtk::d_inliststringlist("name", {"aa", "bb"}, {"aa", "bb", "cc", "dd"});

    // WE set it twice to test the persistence of the mapping through runtime life
    d->setParameters(toto);
    d->setParameters(toto);

    QTextStream ss(&f);
    int stat;
    dtkScriptInterpreterPython::instance()->interpret(ss.readAll(), &stat);
    QVERIFY(stat == 0);
}

void dtkCoreParametersWrappingTestCase::operators(void)
{
    QFile f(QFINDTESTDATA("./operators.py"));
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "cant open python file";
    }

    QTextStream ss(&f);
    int stat;
    dtkScriptInterpreterPython::instance()->interpret(ss.readAll(), &stat);
    QVERIFY(stat == 0);
}

void dtkCoreParametersWrappingTestCase::cleanup(void)
{
}

void dtkCoreParametersWrappingTestCase::cleanupTestCase(void)
{
}

DTKCOREPYTHONTEST_MAIN_NOGUI(dtkCoreParametersWrappingTest, dtkCoreParametersWrappingTestCase);
