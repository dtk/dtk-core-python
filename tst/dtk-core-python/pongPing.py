class TestPongPing(unittest.TestCase):
    """
    Class to test the values set by the C++ code
    """
    def __init__(self):
        super().__init__()
        self.params = {}

    def test_real(self):
        p = self.params["b_real"].value()
        self.assertEqual(p, 1.414, "Should be almost sqt of 2")

    def test_int(self):
        p = self.params["b_int"]
        v = p.value()
        self.assertEqual(v, 1001, "Should be 1001")

    def test_bool(self):
        p = self.params["b_bool"].value()
        self.assertEqual(p, False, "Should be false")

    def test_string(self):
        p = self.params["b_string"].value()
        self.assertEqual(p, "My taylor is rich", "Should be clear")

    def test_inlist(self):
        p = self.params["b_inlist"]
        v = p.value()
        ref = d_inliststring("cell type", "", ["Ganglion", "CNS"], "Select the cell type")
        self.assertEqual(v, ref.value(), "Should be ??")

    def test_range(self):
        p = self.params["b_range"].value()
        r = array_real_2([-1.5, 1.5])
        ref = d_range_real("range", r, -3.0, 3.0)
        self.assertEqual(p[0], ref.value()[0], "Min should be OK")
        self.assertEqual(p[1], ref.value()[1], "Max should be OK")

    def test_inlistslist(self):
        p = self.params["b_inlistslist"].value()
        ref = d_inliststringlist("name", ["aa", "bb"], ["aa", "bb", "cc", "dd"])
        self.assertEqual(p, ref.value(), "Should be OK")

# Retrieve de the factory to get the current instance of the DummyClassImpl

factory = dtk.dummy.factory()
d = factory.current()

# Run the tests to check that values set in the C++ code are correctly handled in the Python part

T = TestPongPing()
T.params = d.parameters()
#print(T.params)
T.test_real()
T.test_int()
T.test_bool()
T.test_string()
T.test_inlist()
T.test_range()
T.test_inlistslist
