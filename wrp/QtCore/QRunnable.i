// QRunnable.i ---

class QRunnable
{
public:
    virtual void run() = 0;
    virtual ~QRunnable();
};

//
// QRunnable.i ends here
