// dtkCore.i ---

#pragma once

#define DTKCORE_EXPORT
%module(directors="1", docstring="dtkCore layer in python", package="dtk") core
%feature("director");


// /////////////////////////////////////////////////////////////////////////////
// Dependencies
// /////////////////////////////////////////////////////////////////////////////

%include <std_array.i>
%import <dtkCore/dtkCoreTypemaps.i>

// /////////////////////////////////////////////////////////////////////////////
// Headers
// /////////////////////////////////////////////////////////////////////////////

%{
#include <QtCore>

#include <dtkCore/dtkCoreMetaType.h>
#include <dtkCore/dtkCoreObjectManager.h>
#include <dtkCore/dtkCorePlugin.h>
#include <dtkCore/dtkCorePluginBase.h>
#include <dtkCore/dtkCorePluginManager.h>
#include <dtkCore/dtkCorePluginFactory.h>
#include <dtkCore/dtkCoreParameter.h>
#include <dtkCore/dtkCoreParameterCollection.h>
#include <dtkCore/dtkCoreParameterInList.h>
#include <dtkCore/dtkCoreParameterInListStringList.h>
#include <dtkCore/dtkCoreParameterNumeric.h>
#include <dtkCore/dtkCoreParameterPath.h>
#include <dtkCore/dtkCoreParameterRange.h>
#include <dtkCore/dtkCoreParameterSimple.h>
#include <dtkCore/dtkCoreParameters.h>
%}

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

%pythonappend dtkCoreObjectManager::value(const QString &key) const  %{
method_name = "to" + val.typeName().strip("*")
try:
    method = getattr(val, method_name)
    return method()
except:
    print("can't convert to " + val.typeName().strip("*") + " type, return a QVariant")
    return val
%}

// /////////////////////////////////////////////////////////////////
// Ignore rules for operators
// /////////////////////////////////////////////////////////////////

%ignore operator>>;
%ignore operator<<;
%ignore operator==;
%ignore operator[];
%ignore operator!=;
%ignore operator*=;
%ignore operator/=;
%ignore operator%=;
%ignore operator bool;
%ignore operator int;
%ignore operator float;
%ignore operator double;
%ignore operator double *;

// /////////////////////////////////////////////////////////////////
// Wrapper input
// /////////////////////////////////////////////////////////////////

%include "dtkCoreMetaType.h"
%include "dtkCoreObjectManager.h"
%include "dtkCorePluginBase.h"
%include "dtkCorePluginManager.h"
%include "dtkCorePluginFactory.h"

// /////////////////////////////////////////////////////////////////
// Wrap dtkCoreParameter
// /////////////////////////////////////////////////////////////////

// Typemaps

%import <dtkCore/dtkCoreParameterTypemaps.i>

// Macro simplifying the wrapping

%define WRAP_DTKCORE_PARAMETER_BASE(name, target_name)
%feature("autodoc", "1");
%feature("nodirector") dtkCoreParameterBase<name>::clone;
%feature("nodirector") dtkCoreParameterBase<name>::metaType;
%feature("nodirector") dtkCoreParameterBase<name>::variant;
%feature("nodirector") dtkCoreParameterBase<name>::copyAndShare;
%template(target_name ## _base) dtkCoreParameterBase<name>;
%enddef

%define WRAP_DTKCORE_PARAMETER_EXTEND(target_name)
%extend dtkCoreParameter {
    dtk:: ## target_name *to_ ## target_name() {
        return dynamic_cast<dtk:: ## target_name *>($self);
  }
}
%enddef

%define WRAP_DTKCORE_PARAMETER(name, target_name)
WRAP_DTKCORE_PARAMETER_BASE(name, target_name)
%template(target_name) name;
WRAP_DTKCORE_PARAMETER_EXTEND(target_name)
%enddef

%define WRAP_DTKCORE_PARAMETER_NO_TEMPLATE(name, target_name)
WRAP_DTKCORE_PARAMETER_BASE(name, target_name)
WRAP_DTKCORE_PARAMETER_EXTEND(target_name)
%rename(target_name) name;
%enddef


// Wrap the parameters provided by dtkCoreParameter.h
// Non template parameters must be before %include section
// because the macro contains %rename function that requires that

%ignore dtkCoreParameterConnection;

%include "dtkCoreParameter.h"

// Numeric parameters

%include "dtkCoreParameterNumeric.h"

// Overload operators

%extend dtkCoreParameterNumeric { // section valid for all template parameters, following comments just use d_real but

    void assign(const self_type& v) { // d_real = d_real.value()
        *($self) = v.value();
    }

    void assign(const value_type& v) { // d_real = double
        *($self) = v;
    }

    self_type& __iadd__(const self_type& v) { // d_real += d_real
        $self->operator += (v);
        return *($self);
    }

    self_type& __iadd__(const value_type& v) { // d_real += double
        $self->operator += (v);
        return *($self);
    }

    auto __add__(const self_type& v) -> value_type { // d_real + d_real -> double
        return $self->value() + v.value();
    }

    auto __add__(const value_type& v) -> value_type { // d_real + double -> double
        return v + *($self);
    }

    auto __radd__(const value_type& v) -> value_type { // double + d_real -> double
        return v + *($self);
    }

    self_type& __isub__(const self_type& v) { // d_real -= d_real
        $self->operator -= (v);
        return *($self);
    }

    self_type& __isub__(const value_type& v) { // d_real -= double
        $self->operator -= (v);
        return *($self);
    }

    auto __sub__(const self_type& v) -> value_type { // d_real - d_real -> double
        return ($self->value() - v.value());
    }

    auto __sub__(const value_type& v) -> value_type { // d_real - double -> double
        return (*($self) - v);
    }

    auto __rsub__(const value_type& v) -> value_type { // double - d_real -> double
        return (v - *($self));
    }

    self_type& __imul__(const self_type& v) { // d_real *= d_real
        $self->operator *= (v);
        return *($self);
    }

    self_type& __imul__(const value_type& v) { // d_real *= double
        $self->operator *= (v);
        return *($self);
    }

    auto __mul__(const self_type& v) -> value_type { // d_real * d_real -> double
        return ($self->value() * v.value());
    }

    auto __mul__(const value_type& v) -> value_type { // d_real * double -> double
        return (*($self) * v);
    }

    auto __rmul__(const value_type& v) -> value_type { // double * d_real -> double
        return (v * *($self));
    }

    self_type& __itruediv__(const self_type& v) { // d_real /= d_real
        $self->operator /= (v);
        return *($self);
    }

    self_type& __itruediv__(const value_type& v) { // d_real /= double
        $self->operator /= (v);
        return *($self);
    }

    auto __truediv__(const self_type& v) -> value_type { // d_real / d_real -> double
        return ($self->value() / v.value());
    }

    auto __truediv__(const value_type& v) -> value_type { // d_real / double -> double
        return (*($self) / v);
    }

    auto __rtruediv__(const value_type& v) -> value_type { // double / d_real -> double
        return (v / *($self));
    }

    self_type& __ifloordiv__(const self_type& v) { // d_real /= d_real -> d_real
        auto r = std::floor($self->value() / v.value());
        $self->setValue(r);
        return *($self);
    }

    self_type& __ifloordiv__(const value_type& v) { // d_real /= double -> d_real
        auto r = std::floor($self->value() / v);
        $self->setValue(r);
        $self->operator /= (v);
        return *($self);
    }

    auto __floordiv__(const self_type& v) -> value_type { // d_real / d_real -> int
        return std::floor($self->value() / v.value());
    }

    auto __floordiv__(const value_type& v) -> value_type { // d_real / double -> int
        return std::floor(*($self) / v);
    }

    auto __rfloordiv__(const value_type& v) -> value_type { // double / d_real -> int
        return std::floor(v / *($self));
    }

    bool __eq__(const self_type& o) {
        return ($self->value() == o.value());
    }

    bool __eq__(const value_type& o) {
        return (*($self) == o);
    }

    bool __req__(const value_type& o) {
        return (*($self) == o);
    }

    bool __neq__(const self_type& o) {
        return ($self->value() != o.value());
    }

    bool __neq__(const value_type& o) {
        return (*($self) != o);
    }

    bool __rneq__(const value_type& o) {
        return (*($self) != o);
    }

	const char* __str__()
	{
        static std::string s;
        auto&& p = $self;
        QString str(p->typeName());
        str += QStringLiteral(" : { label : ") + p->label();
        str += QString(", value : %1, bounds : [%2, %3], decimals : %4, ")
            .arg(p->value()).arg(p->bounds()[0]).arg(p->bounds()[1]).arg(p->decimals());
        str += QStringLiteral("documentation : ") + p->documentation() + QStringLiteral(" }");
        s = str.toStdString();
        return s.data();
	}
 }

%extend dtkCoreParameterNumeric<qlonglong> {

    self_type& __imod__(const self_type& v) { // d_real *= d_real
        auto val = $self->value();
        val %= v.value();
        *($self) = val;
        return *($self);
    }

    self_type& __imod__(const value_type& v) { // d_real *= double
        auto val = $self->value();
        val %= v;
        *($self) = val;
        return *($self);
    }

    auto __mod__(const self_type& v) -> self_type { // d_real * d_real -> d_real
        auto res = decltype(v)(*($self));
        auto val = res.value();
        val %= v.value();
        res = val;
        return res;
    }

    auto __mod__(const value_type& v) -> value_type { // d_real * double -> double
        auto r = decltype(v)(*($self) % v);
        return r;
    }

    auto __rmod__(const value_type& v) -> value_type { // double * d_real -> double
        return v % *($self);
    }
}

WRAP_DTKCORE_PARAMETER(dtkCoreParameterNumeric<char>, d_char)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterNumeric<qulonglong>, d_uint)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterNumeric<qlonglong>, d_int)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterNumeric<double>, d_real)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterNumeric<bool>, d_bool)

// Add specific constructors to avoid SWIG bug with some SFINAE features

%extend dtkCoreParameterNumeric<bool> {
    dtk::d_bool(const QString& label, bool value) {
        dtk::d_bool *p = new dtk::d_bool(value);
        p->setLabel(label);
        return p;
    }
    dtk::d_bool(const QString& label, bool value, const QString& doc) {
        dtk::d_bool *p = new dtk::d_bool(value);
        p->setLabel(label);
        p->setDocumentation(doc);
        return p;
    }
}

%extend dtkCoreParameterNumeric<double> {
    dtk::d_real(const QString& label, double value, double minimum, double maximum, int decimals) {
        dtk::d_real *p = new dtk::d_real(label, value, minimum, maximum, decimals);
        return p;
    }

    dtk::d_real(const QString& label, double value, double minimum, double maximum, int decimals, const QString& doc) {
        dtk::d_real *p = new dtk::d_real(label, value, minimum, maximum, decimals, doc);
        return p;
    }
}

// Simple parameters

%include "dtkCoreParameterSimple.h"

%extend dtkCoreParameterSimple {

	const char* __str__()
	{
        static std::string s;
        auto&& p = $self;
        QString str(p->typeName());
        str += QStringLiteral(" : { label : ") + p->label();
        str += QString(", value : %1, ")
            .arg(p->value());
        str += QStringLiteral("documentation : ") + p->documentation() + QStringLiteral(" }");
        s = str.toStdString();
        return s.data();
	}
}

WRAP_DTKCORE_PARAMETER(dtkCoreParameterSimple<QString>, d_string)

// List parameters

%include "dtkCoreParameterInList.h"

%extend dtkCoreParameterInList {

    const char* __str__()
	{
        static std::string s;
        auto&& p = $self;
        QString str(p->typeName());
        str += QStringLiteral(" : { label : ") + p->label();
        str += QString(", size : %1, value_index : %2, values : [")
            .arg(p->values().size()).arg(p->valueIndex());
        for (qlonglong i = 0; i < p->values().size(); ++i) {
            if (i) {
                str += QStringLiteral(", ");
            }
            str += QString("%1").arg(p->values().at(i));
        }
        str += QStringLiteral("], documentation : ") + p->documentation() + QStringLiteral(" }");
        s = str.toStdString();
        return s.data();
	}
}

WRAP_DTKCORE_PARAMETER(dtkCoreParameterInList<QString>, d_inliststring)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterInList<double>, d_inlistreal)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterInList<qlonglong>, d_inlistint)

// Range parameters

%include "dtkCoreParameterRange.h"

%extend dtkCoreParameterRange {

    auto __getitem__(unsigned i) -> value_type {
        return $self->value()[i];
    }

    const char* __str__()
	{
        static std::string s;
        auto&& p = $self;
        QString str(p->typeName());
        str += QStringLiteral(" : { label : ") + p->label();
        str += QString(", values : [%1, %2], bounds : [%3, %4], decimals : %5, documentation : %6 }")
            .arg(p->value()[0]).arg(p->value()[1])
            .arg(p->bounds()[0]).arg(p->bounds()[1])
            .arg(p->decimals())
            .arg(p->documentation());
        s = str.toStdString();
        return s.data();
	}
}

WRAP_DTKCORE_PARAMETER(dtkCoreParameterRange<unsigned char>, d_range_uchar)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterRange<char>, d_range_char)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterRange<qulonglong>, d_range_uint)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterRange<qlonglong>, d_range_int)
WRAP_DTKCORE_PARAMETER(dtkCoreParameterRange<double>, d_range_real)

// InListStringList parameters

WRAP_DTKCORE_PARAMETER_NO_TEMPLATE(dtkCoreParameterInListStringList, d_inliststringlist)

%include "dtkCoreParameterInListStringList.h"

%extend dtkCoreParameterInListStringList {

    const char* __str__()
	{
        static std::string s;
        auto&& p = $self;
        QString str(p->typeName());
        str += QStringLiteral(" : { label : ") + p->label();
        str += QStringLiteral(", value : [");
        for (int i = 0; i < p->value().size(); ++i) {
            if (i) {
                str += QStringLiteral(", ");
            }
            str += QString("%1").arg(p->value().at(i));
        }
        str += QStringLiteral("], values : [");
        for (int i = 0; i < p->list().size(); ++i) {
            if (i) {
                str += QStringLiteral(", ");
            }
            str += QString("%1").arg(p->list().at(i));
        }
        str += QStringLiteral("], documentation : ") + p->documentation() + QStringLiteral(" }");
        s = str.toStdString();
        return s.data();
	}
}


// Path parameter

WRAP_DTKCORE_PARAMETER_NO_TEMPLATE(dtkCoreParameterPath, d_path)

%include "dtkCoreParameterPath.h"

%extend dtkCoreParameterPath {

    const char* __str__()
	{
        static std::string s;
        auto&& p = $self;
        QString str(p->typeName());
        str += QStringLiteral(" : { label : ") + p->label();
        str += QStringLiteral(", path : ") + p->path();
        str += QStringLiteral(", filters : [");
        for (int i = 0; i < p->filters().size(); ++i) {
            if (i) {
                str += QStringLiteral(", ");
            }
            str += QString("%1").arg(p->filters().at(i));
        }
        str += QStringLiteral("], documentation : ") + p->documentation() + QStringLiteral(" }");
        s = str.toStdString();
        return s.data();
	}
}

// /////////////////////////////////////////////////////////////////
// Wrap other template structures
// /////////////////////////////////////////////////////////////////

%template(array_uchar_2) std::array<unsigned char, 2>;
%template(array_char_2)  std::array<char, 2>;
%template(array_uint_2)  std::array<qulonglong, 2>;
%template(array_int_2)   std::array<qlonglong, 2>;
%template(array_real_2)  std::array<double, 2>;
%template(array_bool_2)  std::array<bool, 2>;

//
// dtkCore.i ends here
