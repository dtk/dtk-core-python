
class TestOperators(unittest.TestCase):
    """
    Class to test the values set by the C++ code
    """
    def __init__(self):
        super().__init__()
        self.params = {}

    def test_assign(self):
        p = self.params["b_real"]
        val = p.value()
        p.assign(1/23)
        self.assertEqual(p.value(), 1/23, "Assign 1")
        pp = d_real(val)
        p.assign(pp)
        self.assertEqual(p.value(), val, "Assign 2")

    def test_add(self):
        p = self.params["b_real"]
        val = p.value()
        p += val
        self.assertEqual(p.value(), 2*val, "Add 1")
        p.assign(val)
        pp = d_real(val)
        p += pp
        self.assertEqual(p.value(), 2*val, "Add 2")
        p.assign(val)
        v = 2 * val
        val = p + pp
        self.assertEqual(val, v, "Add 3")
        v   = p.value()
        val = v + p
        self.assertEqual(val, 2*v, "Add 4")
        val = p + v
        self.assertEqual(val, 2*v, "Add 5")

    def test_sub(self):
        p = self.params["b_real"]
        val = 0.5 * p.value()
        p -= val
        self.assertEqual(p.value(), val, "Sub 1")
        p.assign(2*val)
        pp = d_real(val)
        p -= pp
        self.assertEqual(p.value(), val, "Sub 2")
        p.assign(2*val)
        v = val
        val = p - pp
        self.assertEqual(val, v, "Sub 3")
        v   = p.value() + val
        val = v - p
        self.assertAlmostEqual(val, 0.5*p.value(), 8, "Sub 4")
        val = p - v
        self.assertAlmostEqual(0.5*p.value(), -val, 8, "Sub 5")

    def test_mul(self):
        p = self.params["b_real"]
        val = p.value()
        p *= val
        self.assertEqual(p.value(), val*val, "Mul 1")
        p.assign(val)
        pp = d_real(val)
        p *= pp
        self.assertEqual(p.value(), val*val, "Mul 2")
        p.assign(val)
        v = val * val
        val = p * pp
        self.assertEqual(val, v, "Mul 3")
        v   = p.value()
        val = v * p
        self.assertEqual(val, v*v, "Mul 4")
        val = p * v
        self.assertEqual(val, v*v, "Mul 5")

    def test_div(self):
        p = self.params["b_real"]
        val = p.value()
        p/=val
        self.assertEqual(p.value(), 1, "Div 1")
        p.assign(val)
        pp = d_real(val)
        p /= pp
        self.assertEqual(p.value(), 1, "Div 2")
        p.assign(val)
        val = p / pp
        self.assertEqual(val, 1, "Div 3")
        v   = p.value()
        val = v / p
        self.assertEqual(val, 1, "Div 4")
        val = p / v
        self.assertEqual(val, 1, "Div 5")
        t = 4.2 // 1.3
        p.assign(4.2)
        pp.assign(1.3)
        self.assertEqual(p//pp, t, "Div 6")
        p.assign(4.2)
        pp.assign(1.3)
        p//=pp
        self.assertEqual(p, t, "Div 7")

    def test_mod(self):
        p = self.params["b_int"]
        v = p.value()
        val = p % 101
        self.assertEqual(val, v%101, "Mod 1")
        pp = d_int(101)
        val = p % pp
        self.assertEqual(val, v%101, "Mod 2")
        p %= pp
        self.assertEqual(p.value(), v%101, "Mod 3")
        p.assign(v)
        p %= pp.value()
        self.assertEqual(p.value(), v%101, "Mod 4")
        p.assign(v)
        val = p.value() % pp
        self.assertEqual(val, v%101, "Mod 5")

    def test_eq(self):
        p = self.params["b_int"]
        self.assertTrue(p==1001, "Eq 1")
        pp = d_int(p.value())
        self.assertTrue(p==pp, "Eq 2")

    def test_neq(self):
        p = self.params["b_int"]
        self.assertTrue(p!=1000, "NEq 1")
        pp = d_int(p.value()-1)
        self.assertTrue(p!=pp, "NEq 2")

    def test_range(self):
        p = self.params["b_range"]
        a = p.value()[0]
        b = p.value()[1]
        self.assertEqual(p[0], a, "Range 1")
        self.assertEqual(p[1], b, "Range 2")

# Retrieve de the factory to get the current instance of the DummyClassImpl

factory = dtk.dummy.factory()
d = factory.current()

# Run the tests to check that values set in the C++ code are correctly handled in the Python part

T = TestOperators()
T.params = d.parameters()
T.test_assign()
T.test_add()
T.test_sub()
T.test_mul()
T.test_div()
T.test_mod()
T.test_eq()
T.test_neq()
T.test_range()
