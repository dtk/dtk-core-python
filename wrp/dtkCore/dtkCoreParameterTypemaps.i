// dtkCoreParameters.i ---

#pragma once

// python -> c++

%fragment("ToParameters", "header") {
    void ToParameters(PyObject *obj, dtkCoreParameters& map) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        int r;
        while (PyDict_Next(obj, &pos, &key, &value)) {
            QString k = QString(PyUnicode_AsUTF8(key));
            dtkCoreParameter *v;
            void *s_v = 0;
            r = SWIG_ConvertPtr(value, &s_v, $descriptor(dtkCoreParameter *), 0);
            if (SWIG_IsOK(r)) {
                v = reinterpret_cast<dtkCoreParameter *>(s_v);
                map.insert(k, v);
            } else {
                qInfo() << "SWIG dtkCoreParameters, problem with parameter " << k;
            }
        }
    }
 }

%typemap(in, fragment="ToParameters") dtkCoreParameters {
    if (PyDict_Check($input)) {
        ToParameters($input, $1);
    } else {
        qDebug("PyDict is expected as input. Empty QHash<QString, dtkCoreParameter*> is returned.");
    }
 }

%typemap(in, fragment="ToParameters") const dtkCoreParameters& {
    if (PyDict_Check($input)) {
        $1 = new QHash<QString, dtkCoreParameter *>;
        ToParameters($input, *($1));
    } else {
        qDebug("PyDict is expected as input. Empty QHash<QString, dtkCoreParameter*> is returned.");
    }
 }

%typemap(freearg) const dtkCoreParameters& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout, fragment="ToParameters") dtkCoreParameters {
    PyObject *dict = static_cast<PyObject *>($1);
    if (PyDict_Check(dict)) {
        ToParameters(dict, $result);
    } else {
         qDebug("PyDict is expected as input. Empty QHash<QString, dtkCoreParameter*> is returned.");
    }
}

%typemap(directorout, fragment="ToParameters") const dtkCoreParameters& {
    PyObject *dict = static_cast<PyObject *>($1);
    if (PyDict_Check(dict)) {
        $result = new QHash<QString, dtkCoreParameter *>;
        ToParameters(dict, *($result));
    } else {
         qDebug("PyDict is expected as input. Empty QHash<QString, dtkCoreParameter*> is returned.");
    }
 }

// c++ -> python

%fragment("FromParameters", "header") {
    PyObject *FromParameters(const dtkCoreParameters& map) {
        auto swigTypeInfo = [](dtkCoreParameter *p)
            {
            static QHash<QString, swig_type_info *> mapping;
            auto&& p_type_name = p->typeName();
            swig_type_info *res = nullptr;
            if (!mapping.contains(p_type_name)) {
                res = SWIG_TypeQuery(qPrintable(p_type_name + " *"));
                if (!res) {
                    qDebug() << "Type is unknown" << p_type_name << ", this compiler sucks ?";
                    // ugly workaround: sometimes the compiler use <dtkCoreParameterNumeri<T,void> instead of <dtkCoreParameterNumeri<T>
                    // so, let's try to remove ",void" from qstring ...
                    res = SWIG_TypeQuery(qPrintable(p_type_name.remove(",void") + " *"));
                    if (!res) {
                        qWarning() << "Type is unknown" << p_type_name;
                    }
                }
                mapping[p_type_name] = res;
            } else {
                res = mapping[p_type_name];
            }
            return res;
        };
        auto dict = PyDict_New();
        PyObject *k;
        PyObject *v;
        auto it  = map.cbegin();
        auto end = map.cend();
        for(; it != end; ++it) {
            k = PyUnicode_FromString(it.key().toUtf8().constData());
            Swig::Director *director = SWIG_DIRECTOR_CAST(it.value());
            if (director) {
                v = director->swig_get_self();
                Py_INCREF(v);
            } else {
                auto&& type_info = swigTypeInfo(it.value());
                v = SWIG_NewPointerObj(SWIG_as_voidptr(it.value()), type_info, 0 |  0 );
            }

            PyDict_SetItem(dict, k, v);
        }
        return dict;
    }
}

%typemap(out, fragment="FromParameters") const dtkCoreParameters& {
    $result = FromParameters($1);
}

%typemap(out, fragment="FromParameters") dtkCoreParameters {
    $result = FromParameters($1);
}

%typemap(directorin, fragment="FromParameters") const dtkCoreParameters& {
    $input = FromParameters($1);
}

%typemap(directorin, fragment="FromParameters") dtkCoreParameters {
    $input = FromParameters($1);
}

//
// dtkCoreParameters.i ends here
