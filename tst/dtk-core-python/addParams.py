# This script is called by c++ test addParams
#
# The objective is to add to dummyClass object a parameter of type d_inliststringlist
#
# Eventually, the c++ code will test that the object contains this new params and all its features

my_list = d_inliststringlist("My List", ["tata", "tutu"], ["toto", "tata", "tutu", "titi", "tete"], "Ceci est une liste issue d'une liste")
#print(my_list.value(), my_list.values())

d = dtk.dummy.factory().current()

d.params["a_inliststringlist"] = my_list


