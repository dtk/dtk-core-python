# Changelog

## version 3.4.0 - 2025-03-06
- embed swig interface files previously provided by `dtk-script`
- rename `dtkBase/dtkBase.i` into `dtkCore/dtkCoreTypemaps.i`
- add `setValue(QString)` into `QVariant.i`
- rename `qrunnable.i` into `QRunnable.i`
- add `dtkCoreParameterTypemaps.i` embedding typemaps for `dtkCoreParameters`

## version 3.3.1 - 2025-01-10
- fix swig version

## version 3.3.0 - 2025-01-10
- switch to qt 6.8
- use latest compilers
- modernize cmake and swig management
- new cmake target, namely dtk::CorePython, that has as property the path to dtkCore.i file
- provide a full example in the tst directory that shows how to use this layer.

## version 3.2.0 - 2024-11-19
 - upgrade to dtk-core 3.2
 - first official release for Qt6
 - fix tests for windows
 
## version 2.8.0 - 2021-09-02
 - upgrade to dtk-core 2.11
 - better dtkCreParameter wrapping (operators, hierarchy )
 - swig wrapping with "fragments"

## version 2.7.0 - 2021-08-05
 - fix dtkCoreParameters wrapping
 - new d_inliststringlist parameter
 - new constructor for d_bool

## version 2.6.0 - 2020-04-23
- wrapping of dtkCoreParameter

## version 2.5.6 - 2020-02-27
- fix path in cmakelist

## version 2.5.5 - 2020-02-27
- fix install on windows

## version 2.5.4 - 2019-10-01
- use FindPython3

## version 2.5.3 - 2019-09-20
- initial release
