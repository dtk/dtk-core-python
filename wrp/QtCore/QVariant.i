// QVariant.i ---

class QVariant {
public:
    QVariant();
    QVariant(qlonglong);
    QVariant(const char *);
    QVariant(bool);
    QVariant(double);
    QVariant(const QHash<QString, QVariant> &);
    QVariant(const QMap<QString, QVariant> &);
    QVariant(const QList<QVariant> &);
    QVariant(const QStringList &);
    double toDouble() const;
    long toLongLong() const;
    QHash<QString, QVariant> toHash() const;
    QList<QVariant> toList() const;
    QMap<QString, QVariant> toMap() const;
    QString toString() const;
    QStringList toStringList() const;
    bool toBool() const;
    const char *typeName() const;
};

%extend QVariant {
    void setValue(long value) {
        $self->setValue(QVariant::fromValue(value));
    }

    void setValue(bool value) {
        $self->setValue(QVariant::fromValue(value));
    }

    void setValue(double value) {
        $self->setValue(QVariant::fromValue(value));
    }

    void setValue(QString value) {
        $self->setValue(QVariant::fromValue(value));
    }

    void setValue(const QHash<QString, QVariant>& value) {
        $self->setValue(QVariant::fromValue(value));
    }

    void setValue(const QMap<QString, QVariant>& value) {
        $self->setValue(QVariant::fromValue(value));
    }

    void setValue(const QList<QVariant>& value) {
        $self->setValue(QVariant::fromValue(value));
    }

    void setValue(const QStringList& value) {
        $self->setValue(QVariant::fromValue(value));
    }

    void setValue(QList<long> value) {
        QVariantList varlist;
        for (int i = 0; i < value.length(); i++) {
            varlist.append(QVariant::fromValue(value.at(i)));
        }
        $self->setValue(QVariant::fromValue(varlist));
    }

    void setValue(QList<double> value) {
        QVariantList varlist;
        for (int i = 0; i < value.length(); i++)
            varlist.append(QVariant::fromValue(value.at(i)));
        $self->setValue(QVariant::fromValue(varlist));
    }

    QList<long> toLongList() const{
        QList<long> result;
        if ($self->canConvert<QVariantList>()) {
            QVariantList res_var = $self->toList();
            for (int i = 0; i < res_var.length(); i++)
                result.append(res_var.at(i).toLongLong());
        }
        return result;
    }

    QList<double> toDoubleList() const{
        QList<double> result;
        if ($self->canConvert<QVariantList>()) {
            QVariantList res_var = $self->toList();
            for (int i = 0; i < res_var.length(); i++)
                result.append(res_var.at(i).toDouble());
        }
        return result;
    }
}

//
// QVariant.i ends here
