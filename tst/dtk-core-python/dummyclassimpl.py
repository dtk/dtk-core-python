import unittest
from dtk.core import d_bool, d_int, d_real, d_path, d_inliststringlist, d_range_real, d_inliststring, array_real_2
from dtk.dummy import DummyClass, DummyFactory, DummyPlugin
import dtk.dummy

class DummyClassImpl(DummyClass) :
    """"
    Implementation of the c++ base class DummyClass.

    ...

    Attributes
    ----------
    params : Dict
        the dictionnary storing using (key,value) pattern the dtkCoreParameters

    Methods
    -------

    parameters()
        overrides base class method and return parameters.
        From C++ side, the python Dictionnary is converted into QHash<QString, dtkCoreParameter*> using SWIG typemaps

    addPath(str, dtkCoreParameterPath)
        overrides base class method.
        Adds a dtkCoreParameterPath to the dictionnary
    """
    def __init__(self):
        super().__init__()
        self.params = {}
        self.params["a_bool"] = d_bool("miaou", False)
        self.params["a_int"] = d_int("ll", 1, 0, 10)
        self.params["a_real"] = d_real("rr", 0.3, 0., 100., 1)

    def parameters(self):
        return self.params

    def addPath(self, name, path):
        self.params[name] = path

    def setParameters(self, parameters):
        self.params = parameters

class DummyPluginImpl(DummyPlugin) :
    """
    Implementation of the plugin class DummyPlugin that is registered to the the C++ DummyFactory and that
    is in charge of create the python subclass of the C++ abstrat class DummyClass

    Methods
    -------

    create()
       overrides base class method and return a new instance of DummyClassImpl
    """
    def __init__(self):
        super().__init__()
        self.thisown = 0

    def create(self):
        try:
            obj = DummyClassImpl()
            obj.__disown__()
            return obj
        except Exception as e:
            print(e)
            raise e

# Now the plugin is instantiated and registered to the factory

d = DummyPluginImpl()
factory = dtk.dummy.factory()
factory.record(d)

