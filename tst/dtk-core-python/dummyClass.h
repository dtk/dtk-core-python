// dummyClass.h ---

#pragma once

#include <QtCore>
#include <dtkCore>

#include <DummyExport>

class DUMMY_EXPORT DummyClass
{
public:
    virtual ~DummyClass(void) = default;

    int nbParams(void);
    virtual dtkCoreParameters parameters(void) = 0;
    virtual void setParameters(const dtkCoreParameters&) = 0;

    virtual void addPath(const QString&, dtkCoreParameterPath *) = 0;
};

class DUMMY_EXPORT DummyPlugin
{
public:
    virtual ~DummyPlugin(void) = default;
    virtual void *create(void) { return nullptr; }
};

class DUMMY_EXPORT DummyFactory
{
public:
     DummyFactory(void) = default;
    ~DummyFactory(void) = default;

public:
    void record(DummyPlugin *);
    DummyClass *create(void);
    DummyClass *current(void) const;

private:
    DummyPlugin *m_plugin = nullptr;
    DummyClass *m_current = nullptr;
};

namespace dummy {
    DUMMY_EXPORT DummyFactory *factory(void);
}

//
// dummyClass.h ends here
