#pragma once

%module(directors="1", docstring="Example of module using python module dtk.core", package="dtk") dummy

%{
#include "dummyClass.h"
%}

%import <dtkCore/dtkCore.i>

%feature("director");

#undef  DUMMY_EXPORT
#define DUMMY_EXPORT

%include "dummyClass.h"
