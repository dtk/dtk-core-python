#pragma once

#include <dtkCorePythonTest.h>

class dtkCoreParametersWrappingTestCase : public QObject
{
    Q_OBJECT

public:
     dtkCoreParametersWrappingTestCase(void);
    ~dtkCoreParametersWrappingTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void creation(void);
    void addParams(void);
    void pingPong(void);
    void pongPing(void);
    void operators(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};
