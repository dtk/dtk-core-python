// dtkCoreTypemaps.i ---

#pragma once

%{
#include <QtDebug>
#include <QtCore>
#include <string>
%}

// /////////////////////////////////////////////////////////////////
// Preprocessing setup
// /////////////////////////////////////////////////////////////////

#pragma SWIG nowarn=302, 389, 401, 509, 801, 472, 473, 476, 362, 503, 514, 516, 842, 845

// /////////////////////////////////////////////////////////////////
// Macro undefinition
// /////////////////////////////////////////////////////////////////

#undef Q_OBJECT
#undef signals
#undef slots

#define Q_OBJECT
#define signals public
#define slots

#undef  Q_PROPERTY(Type type MODE mode)
#define Q_PROPERTY(Type type MODE mode)

#undef  Q_DECLARE_INTERFACE(IFace, IId)
#define Q_DECLARE_INTERFACE(IFace, IId)

#undef  Q_DECLARE_METATYPE(Type type)
#define Q_DECLARE_METATYPE(Type type)

#undef  Q_DECL_DEPRECATED
#define  Q_DECL_DEPRECATED

#undef  DTK_DEPRECATED
#define DTK_DEPRECATED

#undef  DTK_DECLARE_PLUGIN(type, Export)
%define DTK_DECLARE_PLUGIN(type, Export)
    class  type##Plugin : public dtkCorePluginBase {
};
%enddef

#undef  DTK_DECLARE_PLUGIN_FACTORY(type, Export)
%define DTK_DECLARE_PLUGIN_FACTORY(type, Export)
%extend QVariant {
    void setValue(type *value) {
        $self->setValue(dtk::variantFromValue(value));
    }
    type* to##type() const {
        return $self->value<type *>();
    }
 }
%template(type##PluginFactorySwigTemplate) dtkCorePluginFactory<type>;
class Export type##PluginFactory : public dtkCorePluginFactory<type> {};
%enddef

#undef  DTK_DECLARE_PLUGIN_MANAGER(type, Export)
%define DTK_DECLARE_PLUGIN_MANAGER(type, Export)
%template(type##PluginManagerSwigTemplate) dtkCorePluginManager<type##Plugin>;
class Export type##PluginManager : public dtkCorePluginManager<type##Plugin> {};
%enddef

#undef  DTK_DECLARE_CONCEPT(type, Export, Namespace)
%define DTK_DECLARE_CONCEPT(type, Export, Namespace)
%rename(Namespace##_pluginFactory) Namespace::pluginFactory;
%rename(Namespace##_pluginManager) Namespace::pluginManager;
namespace Namespace {
     Export type##PluginFactory& pluginFactory();
     Export type##PluginManager& pluginManager();
}
%enddef

#undef  DTK_DECLARE_OBJECT(Type type)
#define DTK_DECLARE_OBJECT(Type type)

#undef  Q_DECL_DEPRECATED_X(msg)
#define Q_DECL_DEPRECATED_X(msg)

// /////////////////////////////////////////////////////////////////
// Ignore rules for operators
// /////////////////////////////////////////////////////////////////

%ignore operator>>;
%ignore operator<<;
%ignore operator==;
%ignore operator[];
%ignore operator!=;
%ignore operator*=;
%ignore operator/=;
%ignore operator bool;
%ignore operator int;
%ignore operator float;
%ignore operator double;
%ignore operator double *;

// /////////////////////////////////////////////////////////////////
// Typemaps
// /////////////////////////////////////////////////////////////////

#ifdef SWIGPYTHON

%typecheck(SWIG_TYPECHECK_STRING) char * {
    $1 = PyUnicode_Check($input) ? 1 : 0;
}

%typemap(in) qlonglong {
   $1 = PyLong_AsLongLong($input);
}

%typemap(in) const qlonglong& {
   qlonglong ii = PyLong_AsLongLong($input);
   $1 = new qlonglong(ii);
}

%typemap(freearg) const qlonglong& {
    if ($1) {
        delete $1;
    }
}
%typemap(directorout) qlonglong {
    PyObject *o = static_cast<PyObject *>($1);
    if (PyLong_Check(o)) {
        $result = PyLong_AsLongLong(o);
    } else {
        qDebug("Long int is expected as input. Zero integer is returned.");
        $result = 0;
    }
}

%typemap(directorout) const qlonglong& {

    PyObject *o = static_cast<PyObject *>($1);
    if (PyLong_Check(o)) {
        $result = new qlonglong(PyLong_AsLongLong(o));
    } else {
        qDebug("Long int is expected as input. Zero integer is returned.");
        $result = new qlonglong;
    }
}

%typemap(typecheck)            qlonglong   = long long;
%typemap(typecheck)      const qlonglong&  = long long;
%typemap(typecheck)       QString  = const char *;
%typemap(typecheck) const QString& = const char *;

%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) QStringList {
    $1 = PyList_Check($input) && PyList_Size($input) && PyUnicode_Check(PyList_GetItem($input, 0));
}
%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) const QStringList& {
    $1 = PyList_Check($input) && PyList_Size($input) && PyUnicode_Check(PyList_GetItem($input, 0));
}

%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) QList<QString> {
    $1 = PyList_Check($input) && PyList_Size($input) && PyUnicode_Check(PyList_GetItem($input, 0));
}
%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) const QList<QString>& {
    $1 = PyList_Check($input) && PyList_Size($input) && PyUnicode_Check(PyList_GetItem($input, 0));
}

%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) QList<long> {
    $1 = PyList_Check($input) && PyList_Size($input) && PyLong_Check(PyList_GetItem($input, 0));
}
%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) const QList<long>& {
    $1 = PyList_Check($input) && PyList_Size($input) && PyLong_Check(PyList_GetItem($input, 0));
}

%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) QList<double> {
    $1 = PyList_Check($input) && PyList_Size($input) && PyFloat_Check(PyList_GetItem($input, 0));
}
%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) const QList<double>& {
    $1 = PyList_Check($input) && PyList_Size($input) && PyFloat_Check(PyList_GetItem($input, 0));
}


%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) QList<QVariant> {
    bool is_list = PyList_Check($input) && PyList_Size($input);
    bool elem_is_variant = false;
    if(is_list) {
        void *void_ptr = 0;
        int res1 = SWIG_ConvertPtr(PyList_GetItem($input, 0), &void_ptr, SWIGTYPE_p_QVariant, 0 |  0 );
        elem_is_variant = void_ptr && SWIG_IsOK(res1);
    }

    $1 = is_list && elem_is_variant;
}
%typemap(typecheck, precedence=SWIG_TYPECHECK_POINTER, noblock=1) const QList<QVariant>& {
    bool is_list = PyList_Check($input) && PyList_Size($input);
    bool elem_is_variant = false;
    if(is_list) {
        void *void_ptr = 0;
        int res1 = SWIG_ConvertPtr(PyList_GetItem($input, 0), &void_ptr, SWIGTYPE_p_QVariant, 0 |  0 );
        elem_is_variant = void_ptr && SWIG_IsOK(res1);
    }

    $1 = is_list && elem_is_variant;
}

// Python -> C++

%typemap(in) QString {
    if (PyUnicode_Check($input)) {
        $1 = QString(PyUnicode_AsUTF8($input));
    } else {
        qDebug("String is expected as input. Empty QString is returned.");
        $1 = QString();
    }
}

%typemap(in) const QString& {
    if (PyUnicode_Check($input)) {
        const char *t = PyUnicode_AsUTF8($input);
        $1 = new QString(t);
    } else {
        qDebug("String is expected as input. Empty QString is returned.");
        $1 = new QString();
    }
}

%typemap(freearg) const QString& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout) QString {
    PyObject *ps = static_cast<PyObject *>($1);
    if (PyUnicode_Check(ps)) {
        $result = QString(PyUnicode_AsUTF8(ps));
    } else {
        qDebug("String is expected as input. Empty QString is returned (directorout).");
        $result = QString();
    }
}

%typemap(directorout) const QString& {
    PyObject *ps = static_cast<PyObject *>($1);
    if (PyUnicode_Check(ps)) {
        const char *t = PyUnicode_AsUTF8(ps);
        $result = new QString(t);
    } else {
        qDebug("String is expected as input. Empty QString is returned.");
        $result = new QString();
    }
}


// c++ -> python
%fragment("FromQVariant", "header") {
    PyObject *FromQVariant(const QVariant& var) {
        int type = var.typeId();
        if (type == QMetaType::Int ||
            type == QMetaType::UInt ||
            type == QMetaType::Long ||
            type == QMetaType::ULong ||
            type == QMetaType::LongLong ||
            type == QMetaType::ULongLong) {
            return PyLong_FromLong(var.value<long>());
        } else if (type == QMetaType::Float ||
                   type == QMetaType::Double) {
            return  PyFloat_FromDouble(var.value<double>());
        } else if (type == QMetaType::QString) {
            return PyUnicode_FromString(qPrintable(var.value<QString>()));
        } else if (type == QMetaType::Bool) {
            bool b = var.value<bool>();
            return b ? Py_True : Py_False;
        } else {
            //Swig::Director *director = SWIG_DIRECTOR_CAST(var);
            PyObject *v;
            //if (director) {
            //    v = director->swig_get_self();
            //    Py_INCREF(v);
            //} else {
                v = SWIG_NewPointerObj(SWIG_as_voidptr(&var), SWIGTYPE_p_QVariant, 0 |  0 );
                return v;
            //}
        }
    }
 }
 // python -> c++
    //typemap in directorout
 %fragment("ToQVariant", "header") {
    void ToQVariant(PyObject *obj, QVariant& v) {
        if (PyUnicode_Check(obj)) {
            v = QVariant::fromValue(QString(PyUnicode_AsUTF8(obj)));
        } else if (PyLong_Check(obj)) {
            v = QVariant::fromValue(PyLong_AsLong(obj));
        } else if (PyFloat_Check(obj)) {
            v = QVariant::fromValue(PyFloat_AsDouble(obj));
        } else if (PyBool_Check(obj)) {
            bool b = (obj == Py_True);
            v = QVariant::fromValue(b);
        } else {
            void *void_ptr = 0;
            int res1 = SWIG_ConvertPtr(obj, &void_ptr, SWIGTYPE_p_QVariant, 0 |  0 );
            if (void_ptr && SWIG_IsOK(res1)) {
                QVariant *v_ptr = reinterpret_cast< QVariant * >(void_ptr);
                v = *v_ptr;
            } else {
                qWarning("Value type is not handled in fragment ToQVariant.");
            }
        }
    }
 }

%typemap(out, fragment="FromQVariant") QVariant {
    $result = FromQVariant($1);
 }

%typemap(out, fragment="FromQVariant") const QVariant& {
    $result = FromQVariant($1);
}

%typemap(directorin, fragment="FromQVariant") QVariant {
    $input = FromQVariant($1);
}

%typemap(directorin, fragment="FromQVariant") const QVariant& {
    $input = FromQVariant($1);
}


%typemap(in, fragment="ToQVariant") QVariant {
    ToQVariant($input, $1);
 }

%typemap(in, fragment="ToQVariant") const QVariant& {
    $1 = new QVariant();
    ToQVariant($input, *($1));
}

%typemap(freearg) const QVariant& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout, fragment="ToQVariant") QVariant {
    ToQVariant($1, $result);
}

%typemap(directorout, fragment="ToQVariant") const QVariant& {
    $result = new QVariant();
    ToQVariant($1, $result);
}

%typemap(in) QStringList {
    if (PyList_Check($input)) {
        qlonglong end = PyList_Size($input);
        for(qlonglong i = 0; i != end; ++i) {
            $1 << QString(PyUnicode_AsUTF8(PyList_GET_ITEM($input, i)));
        }
    } else {
        qDebug("PyList of strings is expected as input. Empty QStringList is returned.");
    }
}

%typemap(in) QList<Qtring> {
    if (PyList_Check($input)) {
        qlonglong end = PyList_Size($input);
        for(qlonglong i = 0; i != end; ++i) {
            $1 << QString(PyUnicode_AsUTF8(PyList_GET_ITEM($input, i)));
        }
    } else {
        qDebug("PyList of strings is expected as input. Empty QList<QString> is returned.");
    }
}

%typemap(in) const QStringList& {
    $1 = new QStringList;
    if (PyList_Check($input)) {
        qlonglong end = PyList_Size($input);
        for(qlonglong i = 0; i != end; ++i) {
            const char *t = PyUnicode_AsUTF8(PyList_GET_ITEM($input, i));
            (*$1) << QString(t);
        }
    } else {
        qDebug("PyList of strings is expected as input. Empty QStringList is returned.");
    }
}

%typemap(in) const QList<QString>& {
    $1 = new QList<QString>;
    if (PyList_Check($input)) {
        qlonglong end = PyList_Size($input);
        for(qlonglong i = 0; i != end; ++i) {
            const char *t = PyUnicode_AsUTF8(PyList_GET_ITEM($input, i));
            (*$1) << QString(t);
        }
    } else {
        qDebug("PyList of strings is expected as input. Empty QList<QString> is returned.");
    }
}


%typemap(freearg) const QStringList& {
    if ($1) {
        delete $1;
    }
}

%typemap(freearg) const QList<QString>& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout) QStringList {
    PyObject *list = static_cast<PyObject *>($1);
    if (PyList_Check(list)) {
        qlonglong end = PyList_Size(list);
        for(qlonglong i = 0; i<end; ++i) {
            const char *t = PyUnicode_AsUTF8(PyList_GET_ITEM(list, i));
            $result << QString(t);
        }
    } else {
        qDebug("PyList of string is expected as input. Empty QStringList is returned.");
    }
}

%typemap(directorout) QList<QString> {
    PyObject *list = static_cast<PyObject *>($1);
    if (PyList_Check(list)) {
        qlonglong end = PyList_Size(list);
        for(qlonglong i = 0; i<end; ++i) {
            const char *t = PyUnicode_AsUTF8(PyList_GET_ITEM(list, i));
            $result << QString(t);
        }
    } else {
        qDebug("PyList of string is expected as input. Empty QList<QString> is returned.");
    }
}

%typemap(directorout) const QStringList& {
    PyObject *list = static_cast<PyObject *>($1);
    $result = new QStringList;
    if (PyList_Check(list)) {
        qlonglong end = PyList_Size(list);
        for(qlonglong i = 0; i<end; ++i) {
            const char *t = PyUnicode_AsUTF8(PyList_GET_ITEM(list, i));
            (*$result) << QString(t);
        }
    } else {
        qDebug("PyList of string is expected as input. Empty QList<QString> is returned.");
    }
}

%typemap(directorout) const QList<QString>& {
    PyObject *list = static_cast<PyObject *>($1);
    $result = new QList<QString>;
    if (PyList_Check(list)) {
        qlonglong end = PyList_Size(list);
        for(qlonglong i = 0; i<end; ++i) {
            const char *t = PyUnicode_AsUTF8(PyList_GET_ITEM(list, i));
            (*$result) << QString(t);
        }
    } else {
        qDebug("PyList of string is expected as input. Empty QList<QString> is returned.");
    }
}

%typemap(in) QList<long> {
    if (PyList_Check($input)) {
        qlonglong end = PyList_Size($input);
        for(qlonglong i = 0; i != end; ++i) {
            $1 << PyLong_AsLong(PyList_GET_ITEM($input, i));
        }
    } else {
        qDebug("PyList of integers is expected as input. Empty QList<long> is returned.");
    }
}

%typemap(in) const QList<long>& {
    $1 = new QList<long>;
    if (PyList_Check($input)) {
        qlonglong end = PyList_Size($input);
        for(qlonglong i = 0; i != end; ++i) {
            ($1)->append(PyLong_AsLong(PyList_GET_ITEM($input, i)));
        }
    } else {
        qDebug("PyList of integers is expected as input. Empty QList<long> is returned.");
    }
}

%typemap(freearg) const QList<long>& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout) QList<long> {
    PyObject *list = static_cast<PyObject *>($1);
    if (PyList_Check(list)) {
        qlonglong end = PyList_Size(list);
        for(qlonglong i = 0; i < end; ++i) {
            PyObject *o = PyList_GET_ITEM(list, i);
            $result << PyLong_AsLong(o);
        }
    } else {
        qDebug("PyList of integers is expected as input. Empty QList<long> is returned.");
    }
}


%typemap(in) QList<unsigned long> {
    if (PyList_Check($input)) {
        qlonglong i = 0;
        qlonglong end = PyList_Size($input);
        for(i;i!=end; ++i) {
            $1 << PyLong_AsLong(PyList_GET_ITEM($input, i));
        }
    } else {
        qDebug("PyList of integers is expected as input. Empty QList<unsigned long> is returned.");
    }
}

%typemap(in) const QList<unsigned long>& {
    $1 = new QList<unsigned long>;
    if (PyList_Check($input)) {
        qlonglong i = 0;
        qlonglong end = PyList_Size($input);
        for(i;i!=end; ++i) {
            ($1)->append(PyLong_AsLong(PyList_GET_ITEM($input, i)));
        }
    } else {
        qDebug("PyList of integers is expected as input. Empty QList<unsigned long> is returned.");
    }
}

%typemap(freearg) const QList<unsigned long>& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout) QList<unsigned long> {
    PyObject *list = static_cast<PyObject *>($1);
    if (PyList_Check(list)) {
        qlonglong i = 0;
        qlonglong end = PyList_Size(list);
        for(i;i<end; ++i) {
            PyObject *o = PyList_GET_ITEM(list, i);
            $result << PyLong_AsLong(o);
        }
    } else {
        qDebug("PyList of integers is expected as input. Empty QList<unsigned long> is returned.");
    }
}

%typemap(out) QList<unsigned long> {
    $result = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject* v = PyLong_FromLong(*it);
        PyList_SET_ITEM($result, i, v);
    }
}

%typemap(directorin) QList<unsigned long> {
    PyObject *list = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject* v = PyLong_FromLong(*it);
        PyList_SET_ITEM(list, i, v);
    }
    $input = list;
}


%typemap(in) QList<double> {
    if (PyList_Check($input)) {
        qlonglong i = 0;
        qlonglong end = PyList_Size($input);
        for(;i!=end; ++i) {
            $1 << PyFloat_AsDouble(PyList_GET_ITEM($input, i));
        }
    } else {
        qDebug("PyList of double is expected as input. Empty QList<double> is returned.");
    }
}

%typemap(in) const QList<double>& {
    $1 = new QList<double>;
    if (PyList_Check($input)) {
        qlonglong i = 0;
        qlonglong end = PyList_Size($input);
        for(;i!=end; ++i) {
            ($1)->append(PyFloat_AsDouble(PyList_GET_ITEM($input, i)));
        }
    } else {
        qDebug("PyList of double floats is expected as input. Empty QList<double> is returned.");
    }
}

%typemap(freearg) const QList<double>& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout) QList<double> {
    PyObject *list = static_cast<PyObject *>($1);
    if (PyList_Check(list)) {
        qlonglong i = 0;
        qlonglong end = PyList_Size(list);
        for(;i<end; ++i) {
            PyObject *o = PyList_GET_ITEM(list, i);
            $result << PyFloat_AsDouble(o);
        }
    } else {
        qDebug("PyList of double floats is expected as input. Empty QList<double> is returned.");
    }
}

%typemap(out) QList<double> {
    $result = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject* v = PyFloat_FromDouble(*it);
        PyList_SET_ITEM($result, i, v);
    }
}

%typemap(directorin) QList<double> {
    PyObject *list = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject* v = PyFloat_FromDouble(*it);
        PyList_SET_ITEM(list, i, v);
    }
    $input = list;
}

%typemap(in, fragment="ToQVariant") QMap<long, QVariant> {
    if (PyDict_Check($input)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        QVariant v;
        while (PyDict_Next($input, &pos, &key, &value)) {
            long k = PyLong_AS_LONG(key);
            ToQVariant(value, v);
            $1.insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QMap<long, QVariant> is returned.");
    }
}

%typemap(in, fragment="ToQVariant") const QMap<long, QVariant>& {
    $1 = new QMap<long, QVariant>;
    if (PyDict_Check($input)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        QVariant v;
        while (PyDict_Next($input, &pos, &key, &value)) {
            long k = PyLong_AS_LONG(key);
            ToQVariant(value, v);
            ($1)->insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QMap<long, QVariant> is returned.");
    }
}

%typemap(freearg) const QMap<long, QVariant>& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout, fragment="ToQVariant") QMap<long, QVariant> {
    PyObject *dict = static_cast<PyObject *>($1);
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        QVariant v;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            long k = PyLong_AS_LONG(key);
            ToQVariant(value, v);
            $result.insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QMap<long, QVariant> is returned.");
    }
}

%typemap(directorout, fragment="ToQVariant") const QMap<long, QVariant>& {
    PyObject *dict = static_cast<PyObject *>($1);
    $result = new QMap<long, QVariant>();
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        QVariant v;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            long k = PyLong_AS_LONG(key);
            ToQVariant(value, v);
            $result->insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QMap<long, QVariant> is returned.");
    }
}

%typemap(directorout, fragment="ToQVariant") QMap<long, QVariant>& {
    PyObject *dict = static_cast<PyObject *>($1);
    $result = new QMap<long, QVariant>();
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        QVariant v;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            long k = PyLong_AS_LONG(key);
            ToQVariant(value, v);
            $result->insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QMap<long, QVariant> is returned.");
    }
}

// C++ -> Python

%typemap(out) qlonglong {
   $result = PyLong_FromLongLong($1);
}

%typemap(out) const qlonglong& {
   $result = PyLong_FromLongLong(*$1);
}

%typemap(directorin) qlonglong {
    $input = PyLong_FromLongLong($1);
}

%typemap(directorin) const qlonglong& {
    $input = PyLong_FromLongLong($1);
}

%typemap(out) QString {
    $result = PyUnicode_FromString($1.toUtf8().constData());
}

%typemap(out) const QString& {
    QString *s = const_cast<QString*>($1);
    $result = PyUnicode_FromString(s->toUtf8().constData());
}

%typemap(directorin) QString {
    $input = PyUnicode_FromString($1.toUtf8().constData());
}

%typemap(directorin) const QString& {
    $input = PyUnicode_FromString($1.toUtf8().constData());
}

%define %QList_typemapsPtr(DATA_TYPE)

%typemap(out) QList<DATA_TYPE> {
    $result = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0 ; it != end; ++it, ++i)  {
        PyObject *obj = SWIG_NewPointerObj((*it), $descriptor(DATA_TYPE), 0|0);
        PyList_SET_ITEM($result, i, obj);
    }
}

%typemap(directorin) QList<DATA_TYPE> {
    PyObject *list = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0 ; it != end; ++it, ++i)  {
        PyObject *obj = SWIG_NewPointerObj((*it), $descriptor(DATA_TYPE), 0|0);
        PyList_SET_ITEM(list, i, obj);
    }
    $input = list;
}

%enddef // %QList_typemapsPtr()

%QList_typemapsPtr(dtkCorePlugin *)

%define %QList_typemaps(DATA_TYPE)

%typemap(out) QList<DATA_TYPE> {
    $result = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0 ; it != end; ++it, ++i)  {
        DATA_TYPE *newItem = new DATA_TYPE(*it);
        PyObject *obj = SWIG_NewPointerObj(newItem, $descriptor(DATA_TYPE*), 0|0);
        PyList_SET_ITEM($result, i, obj);
    }
}

%typemap(directorin) QList<DATA_TYPE> {
    PyObject *list = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0 ; it != end; ++it, ++i)  {
        DATA_TYPE *newItem = new DATA_TYPE(*it);
        PyObject* obj = SWIG_NewPointerObj(newItem, $descriptor(DATA_TYPE*), 0|0);
        PyList_SET_ITEM(list, i, obj);
    }
    $input = list;
}

%enddef // %QList_typemaps()

%typemap(out) QStringList {
    $result = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject *st = PyUnicode_FromString((*it).toUtf8().constData());
        PyList_SET_ITEM($result, i, st);
    }
}

%typemap(out) QList<QString> {
    $result = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject *st = PyUnicode_FromString((*it).toUtf8().constData());
        PyList_SET_ITEM($result, i, st);
    }
}

%typemap(directorin) QStringList {
    PyObject *list = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject *st = PyUnicode_FromString((*it).toUtf8().constData());
        PyList_SET_ITEM(list, i, st);
    }
    $input = list;
}

%typemap(directorin) QList<QString> {
    PyObject *list = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject *st = PyUnicode_FromString((*it).toUtf8().constData());
        PyList_SET_ITEM(list, i, st);
    }
    $input = list;
}

%typemap(out) QList<long> {
    $result = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject* v = PyLong_FromLong(*it);
        PyList_SET_ITEM($result, i, v);
    }
}

%typemap(directorin) QList<long> {
    PyObject *list = PyList_New($1.size());
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(qlonglong i = 0; it != end; ++it, ++i) {
        PyObject* v = PyLong_FromLong(*it);
        PyList_SET_ITEM(list, i, v);
    }
    $input = list;
}

%typemap(out, fragment="FromQVariant") QMap<long, QVariant> {
    $result = PyDict_New();
    PyObject *k;
    PyObject *v;
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(; it != end; ++it) {
        k = PyLong_FromLong(it.key());
        QVariant val = it.value();
        v = FromQVariant(val);
        PyDict_SetItem($result, k, v);
    }
}

%typemap(directorin, fragment="FromQVariant") QMap<long, QVariant> {
    PyObject *map = PyDict_New();
    PyObject *k;
    PyObject *v;
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(; it != end; ++it) {
        k = PyLong_FromLong(it.key());
        QVariant val = it.value();
        v = FromQVariant(val);
        PyDict_SetItem(map, k, v);
    }
    $input = map;
}


%typemap(directorin, fragment="FromQVariant") QVariantMap {
    PyObject *map = PyDict_New();
    PyObject *k;
    PyObject *v;
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(; it != end; ++it) {
        k = PyUnicode_FromString(it.key());
        QVariant val = it.value();
        v = FromQVariant(val);
        PyDict_SetItem(map, k, v);
    }
    $input = map;
}

%typemap(out, fragment="FromQVariant") const QVariantMap& {
    PyObject *map = PyDict_New();
    PyObject *k;
    PyObject *v;
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(; it != end; ++it) {
        k = PyUnicode_FromString(qPrintable(it.key()));
        QVariant val = it.value();
        v = FromQVariant(val);
        PyDict_SetItem(map, k, v);
    }
    $result = map;
}

%typemap(directorin, fragment="FromQVariant") const QVariantMap& {
    PyObject *map = PyDict_New();
    PyObject *k;
    PyObject *v;
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(; it != end; ++it) {
        k = PyUnicode_FromString(qPrintable(it.key()));
        QVariant val = it.value();
        v = FromQVariant(val);
        PyDict_SetItem(map, k, v);
    }
    $input = map;
}

%typemap(directorout, fragment="ToQVariant") QVariantMap {
    PyObject *dict = static_cast<PyObject *>($1);
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            QString k = QString(PyUnicode_AsUTF8(key));
            QVariant v;
            ToQVariant(value, v);
            $result.insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QVariantMap is returned.");
    }
 }


%typemap(in, fragment="ToQVariant") const QVariantMap& {
    $1 = new QVariantMap;

    if (PyDict_Check($input)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        while (PyDict_Next($input, &pos, &key, &value)) {
            QString k = QString(PyUnicode_AsUTF8(key));
            QVariant v;
            ToQVariant(value, v);
            ($1)->insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QVariantMap is returned.");
    }
}

%typemap(freearg) const QVariantMap& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout, fragment="ToQVariant") const QVariantMap& {
    PyObject *dict = static_cast<PyObject *>($1);
    $result = new QVariantMap;
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            QString k = QString(PyUnicode_AsUTF8(key));
            QVariant v;
            ToQVariant(value, v);
            $result->insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QVariantMap is returned.");
    }
 }


%typemap(directorin, fragment="FromQVariant") QVariantHash {
    PyObject *map = PyDict_New();
    PyObject *k;
    PyObject *v;
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(; it != end; ++it) {
        k = PyUnicode_FromString(it.key());
        QVariant val = it.value();
        v = FromQVariant(val);
        PyDict_SetItem(map, k, v);
    }
    $input = map;
}

%typemap(directorin, fragment="FromQVariant") const QVariantHash& {
    PyObject *map = PyDict_New();
    PyObject *k;
    PyObject *v;
    auto it  = $1.cbegin();
    auto end = $1.cend();
    for(; it != end; ++it) {
        k = PyUnicode_FromString(qPrintable(it.key()));
        QVariant val = it.value();
        v = FromQVariant(val);
        PyDict_SetItem(map, k, v);
    }
    $input = map;
}

%typemap(freearg) const QVariantHash& {
    if ($1) {
        delete $1;
    }
}

%typemap(directorout, fragment="ToQVariant") QVariantHash {
    PyObject *dict = static_cast<PyObject *>($1);
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            QString k = QString(PyUnicode_AsUTF8(key));
            QVariant v;
            ToQVariant(value, v);
            $result.insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QVariantHash is returned.");
    }
 }

%typemap(in, fragment="ToQVariant") QVariantHash {
    PyObject *dict = static_cast<PyObject *>($input);
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            QString k = QString(PyUnicode_AsUTF8(key));
            QVariant v;
            ToQVariant(value, v);
            $1.insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QVariantHash is returned.");
    }
 }

%typemap(in, fragment="ToQVariant") const QVariantHash& {
    PyObject *dict = static_cast<PyObject *>($input);
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            QString k = QString(PyUnicode_AsUTF8(key));
            QVariant v;
            ToQVariant(value, v);
            ($1)->insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QVariantHash is returned.");
    }
 }

%typemap(directorout, fragment="ToQVariant") const QVariantHash& {
    PyObject *dict = static_cast<PyObject *>($1);
    if (PyDict_Check(dict)) {
        PyObject *key, *value;
        Py_ssize_t pos = 0;
        while (PyDict_Next(dict, &pos, &key, &value)) {
            QString k = QString(PyUnicode_AsUTF8(key));
            QVariant v;
            ToQVariant(value, v);
            $result.insert(k, v);
        }
    } else {
        qDebug("PyDict is expected as input. Empty QVariantHash is returned.");
    }
 }
// QPair

template <class T1, class T2> class QPair
{
public:
    T1 first;
    T2 second;
};

%define %QPair_typemaps(DATA_TYPE_1, DATA_TYPE_2)

%typemap(out) QPair<DATA_TYPE_1, DATA_TYPE_2> {
    $result = PyTuple_New(2);
    PyObject* obj1 = SWIG_NewPointerObj(*$1.first, $descriptor(DATA_TYPE_1), 0|0);
    PyObject* obj2 = SWIG_NewPointerObj(*$1.second, $descriptor(DATA_TYPE_2), 0|0);
    PyTuple_SET_ITEM($result, 0, obj1);
    PyTuple_SET_ITEM($result, 1, obj2);
}

%enddef // %QPair_typemaps()

%template(QPairStrStrList) QPair<QString, QStringList>;

%QPair_typemaps(QString, QStringList)

#elif SWIGTCL

// Tcl -> C++

%typemap(in) QString {
    $1 = QString(Tcl_GetString($input));
}

%typemap(in) const QString& {
    char *t = Tcl_GetString($input);
    $1 = new QString(t);
}

// C++ -> Tcl

%typemap(out) QString {
    Tcl_SetStringObj($result, $1.toUtf8().constData(), $1.size());
}

%typemap(out) const QString& {
    Tcl_SetStringObj($result, $1.toUtf8().constData(), $1.size());
}

#elif SWIGCSHARP

// /////////////////////////////////////////////////////////////////
// C#
// /////////////////////////////////////////////////////////////////

%typemap(ctype) QString "char *"
%typemap(imtype) QString "string"
%typemap(cstype) QString "string"
%typemap(csdirectorin) QString "$iminput"
%typemap(csdirectorout) QString "$cscall"

%typemap(in, canthrow=1) QString {
    if (!$input) {
        SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "null string", 0);
        return $null;
    }

    $1 = QString($input);
}

%typemap(out) QString {
    $result = SWIG_csharp_string_callback($1.toUtf8().constData());
}

%typemap(directorout, canthrow=1) QString {
    if (!$input) {
        SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "null string", 0);
        return $null;
    }

    $result = string($input);
}

%typemap(directorin) QString {
    $input = SWIG_csharp_string_callback($1.toUtf8().constData());
}

%typemap(csin) QString "$csinput"

%typemap(csout, excode=SWIGEXCODE) QString {
    string ret = $imcall;$excode;
    return ret;
}

%typemap(csvarin, excode=SWIGEXCODE2) QString {
    set {
      $imcall;$excode
    }
}

%typemap(csvarout, excode=SWIGEXCODE2) QString {
    get {
      string ret = $imcall;$excode
      return ret;
    }
}

%typemap(typecheck) QString = char *;

%typemap(throws, canthrow=1) QString {
    SWIG_CSharpSetPendingException(SWIG_CSharpApplicationException, $1.c_str());
    return $null;
}

%typemap(ctype) const QString & "char *"
%typemap(imtype) const QString & "string"
%typemap(cstype) const QString & "string"
%typemap(csdirectorin) const QString & "$iminput"
%typemap(csdirectorout) const QString & "$cscall"

%typemap(in, canthrow=1) const QString & {
    if (!$input) {
        SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "null string", 0);
        return $null;
    }
    QString $1_str($input);
    $1 = &$1_str;
}

%typemap(out) const QString & {
    $result = SWIG_csharp_string_callback($1.toUtf8().constData());
}

%typemap(csin) const QString & "$csinput"

%typemap(csout, excode=SWIGEXCODE) const QString & {
    string ret = $imcall;$excode
    return ret;
}

%typemap(directorout, canthrow=1, warning=SWIGWARN_TYPEMAP_THREAD_UNSAFE_MSG) const QString & {
    if (!$input) {
        SWIG_CSharpSetPendingExceptionArgument(SWIG_CSharpArgumentNullException, "null string", 0);
        return $null;
   }
   static string $1_str;
   $1_str = $input;
   $result = &$1_str;
}

%typemap(directorin) const QString & {
    $input = SWIG_csharp_string_callback($1.toUtf8().constData());
}

%typemap(csvarin, excode=SWIGEXCODE2) const QString & {
    set {
      $imcall;$excode
    }
}

%typemap(csvarout, excode=SWIGEXCODE2) const QString & {
    get {
        string ret = $imcall;$excode;
        return ret;
    }
}

%typemap(typecheck) const QString & = char *;

%typemap(throws, canthrow=1) const QString & {
    SWIG_CSharpSetPendingException(SWIG_CSharpApplicationException, $1.c_str());
    return $null;
}

#endif

//
// dtkCoreTypemaps.i ends here
