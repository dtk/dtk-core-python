class TestPingPong(unittest.TestCase):
    """
    Class to test the values set by the C++ code
    """
    def __init__(self):
        super().__init__()
        self.params = {}

    def test_real(self):
        p = self.params["a_real"].value()
        self.assertEqual(p, 3.14159, "Should be almost Pi")

    def test_int(self):
        p = self.params["a_int"].value()
        self.assertEqual(p, 9, "Should be 9")

    def test_bool(self):
        p = self.params["a_bool"].value()
        self.assertEqual(p, True, "Should be True")

# Retrieve de the factory to get the current instance of the DummyClassImpl

factory = dtk.dummy.factory()
d = factory.current()

# Run the tests to check that values set in the C++ code are correctly handled in the Python part

T = TestPingPong()
T.params = d.parameters()
#print(T.params)
T.test_real()
T.test_int()
T.test_bool()

# Set new values that will be tested in the C++ part

r = d.params["a_real"]
r.setValue(1.414)

i = d.params["a_int"]
i.setValue(i.value() - 6)

b = d.params["a_bool"]
b.setValue(False)
